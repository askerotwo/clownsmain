﻿using System;
using System.Collections.Generic;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace ExcelReader
{
    class ExcelReader
    {
        public void readExcel(List<Action> actions)
        {
            Excel.Application objExcel = new Excel.Application();
            Excel.Worksheet workSheet;

            String path = Directory.GetCurrentDirectory() + "\\actdata.xlsx";
            System.Console.WriteLine(path);

            Excel.Workbook workBook = objExcel.Workbooks.Open(
                path, 0, true, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            workSheet = (Excel.Worksheet)workBook.Worksheets.get_Item(1);

            Excel.Range range = workSheet.UsedRange;
            int cntRows = range.Rows.Count;
            int cntColumns = range.Columns.Count;
            Console.WriteLine(cntRows);
            Console.WriteLine(cntColumns);
            for (int row = 2; row < cntColumns; row++)
            {
                Excel.Range name = (Excel.Range)workSheet.Cells[row, 2];
                Excel.Range type = (Excel.Range)workSheet.Cells[row, 3];
                Excel.Range message = (Excel.Range)workSheet.Cells[row, 4];
                Excel.Range assoc = (Excel.Range)workSheet.Cells[row, 5];


                Excel.Range mult = (Excel.Range)workSheet.Cells[row, 6];
                Excel.Range prior = (Excel.Range)workSheet.Cells[row, 7];
                Excel.Range w1 = (Excel.Range)workSheet.Cells[row, 8];
                Excel.Range v1 = (Excel.Range)workSheet.Cells[row, 9];
                Excel.Range a1 = (Excel.Range)workSheet.Cells[row, 10];
                Excel.Range d1 = (Excel.Range)workSheet.Cells[row, 11];
                Excel.Range w2 = (Excel.Range)workSheet.Cells[row, 12];
                Excel.Range v2 = (Excel.Range)workSheet.Cells[row, 13];
                Excel.Range a2 = (Excel.Range)workSheet.Cells[row, 14];
                Excel.Range d2 = (Excel.Range)workSheet.Cells[row, 15];


                Excel.Range atype = (Excel.Range)workSheet.Cells[row, 16];
                Excel.Range astate1 = (Excel.Range)workSheet.Cells[row, 17];
                Excel.Range apos1 = (Excel.Range)workSheet.Cells[row, 18];
                Excel.Range apre = (Excel.Range)workSheet.Cells[row, 19];
                Excel.Range astate2 = (Excel.Range)workSheet.Cells[row, 20];
                Excel.Range apos2 = (Excel.Range)workSheet.Cells[row, 21];
                Excel.Range apost = (Excel.Range)workSheet.Cells[row, 22];


                Excel.Range otype = (Excel.Range)workSheet.Cells[row, 23];
                Excel.Range ostate1 = (Excel.Range)workSheet.Cells[row, 24];
                Excel.Range opos1 = (Excel.Range)workSheet.Cells[row, 25];
                Excel.Range opre = (Excel.Range)workSheet.Cells[row, 26];
                Excel.Range ostate2 = (Excel.Range)workSheet.Cells[row, 27];
                Excel.Range opos2 = (Excel.Range)workSheet.Cells[row, 28];
                Excel.Range opost = (Excel.Range)workSheet.Cells[row, 29];


                Excel.Range ttype = (Excel.Range)workSheet.Cells[row, 30];
                Excel.Range tstate1 = (Excel.Range)workSheet.Cells[row, 31];
                Excel.Range tpos1 = (Excel.Range)workSheet.Cells[row, 32];
                Excel.Range tpre = (Excel.Range)workSheet.Cells[row, 33];
                Excel.Range tstate2 = (Excel.Range)workSheet.Cells[row, 34];
                Excel.Range tpos2 = (Excel.Range)workSheet.Cells[row, 35];
                Excel.Range tpost = (Excel.Range)workSheet.Cells[row, 36];

                actions.Add(new Action((string)name.Value2, (string)type.Value2, (string)message.Value2, (string)assoc.Value2,
                    new VAD((double)mult.Value2, (double)prior.Value2,
                    new double[4] { Convert.ToDouble(w1.Value2), Convert.ToDouble(v1.Value2), Convert.ToDouble(a1.Value2), Convert.ToDouble(d1.Value2) },
                    new double[4] { Convert.ToDouble(w2.Value2), Convert.ToDouble(v2.Value2), Convert.ToDouble(a2.Value2), Convert.ToDouble(d2.Value2) }),
                    new ActionActor((string)atype.Value2, (string)astate1.Value2, (string)apos1.Value2, (string)apre.Value2, (string)astate2.Value2, (string)apos2.Value2, (string)apost.Value2),
                    new ActionActor((string)otype.Value2, (string)ostate1.Value2, (string)opos1.Value2, (string)opre.Value2, (string)ostate2.Value2, (string)opos2.Value2, (string)opost.Value2),
                    new ActionActor((string)ttype.Value2, (string)tstate1.Value2, (string)tpos1.Value2, (string)tpre.Value2, (string)tstate2.Value2, (string)tpos2.Value2, (string)tpost.Value2))
                    );
            }
        }
        public static void Main(string[] args)
        {

            Excel.Application objExcel = new Excel.Application();
            ExcelReader excelReader = new ExcelReader();

            List<Action> actions = new List<Action>(69);

            excelReader.readExcel(actions);
            //Console.WriteLine(actions[0].name);
            foreach (var v in actions)
            {
                Console.WriteLine(v.name);
                Console.WriteLine(v.type);
                Console.WriteLine(v.message);
                Console.WriteLine(v.assoc);
                Console.WriteLine(v.vad.mult);
                Console.WriteLine(v.vad.prior);
                Console.WriteLine(v.vad.author[0]);
                Console.WriteLine(v.vad.author[1]);
                Console.WriteLine(v.vad.author[2]);
                Console.WriteLine(v.vad.author[3]);
                Console.WriteLine(v.vad.target[0]);
                Console.WriteLine(v.vad.target[1]);
                Console.WriteLine(v.vad.target[2]);
                Console.WriteLine(v.vad.target[3]);
                Console.WriteLine(v.author.type);
                Console.WriteLine(v.author.stateBefore);
                Console.WriteLine(v.author.posBefore);
                Console.WriteLine(v.author.preBefore);
                Console.WriteLine(v.author.stateAfter);
                Console.WriteLine(v.author.posAfter);
                Console.WriteLine(v.author.postAfter);
                Console.WriteLine(v.author.type);

                Console.WriteLine(v.thing.stateBefore);
                Console.WriteLine(v.thing.posBefore);
                Console.WriteLine(v.thing.preBefore);
                Console.WriteLine(v.thing.stateAfter);
                Console.WriteLine(v.thing.posAfter);
                Console.WriteLine(v.thing.postAfter);
                Console.WriteLine(v.thing.type);

                Console.WriteLine(v.target.stateBefore);
                Console.WriteLine(v.target.posBefore);
                Console.WriteLine(v.target.preBefore);
                Console.WriteLine(v.target.stateAfter);
                Console.WriteLine(v.target.posAfter);
                Console.WriteLine(v.target.postAfter);
                Console.WriteLine("----------------------------");
            }
            //SystemConsole.Log();
            Console.ReadKey();

        }
    }
    class Action
    {

        public string name;
        public string type;
        public string message;
        public string assoc;
        public VAD vad;
        public ActionActor author;
        public ActionActor thing;
        public ActionActor target;

        public Action(string name, string type, string message, string assoc, VAD vad, ActionActor author, ActionActor thing, ActionActor target)
        {
            this.name = name;
            this.type = type;
            this.message = message;
            this.assoc = assoc;
            this.vad = vad;
            this.author = author;
            this.thing = thing;
            this.target = target;
        }
    }

    class VAD
    {
        public double mult;
        public double prior;
        public double[] author = new double[4];
        public double[] target = new double[4];

        public VAD(double mult, double prior, double[] author, double[] target)
        {
            this.mult = mult;
            this.prior = prior;
            this.author = author;
            this.target = target;
        }
    }

    class ActionActor
    {
        public string type;
        public string stateBefore;
        public string posBefore;
        public string preBefore;
        public string stateAfter;
        public string posAfter;
        public string postAfter;

        public ActionActor(string type, string stateBefore, string posBefore, string preBefore, string stateAfter, string posAfter, string postAfter)
        {
            this.type = type;
            this.stateBefore = stateBefore;
            this.posBefore = posBefore;
            this.preBefore = preBefore;
            this.stateAfter = stateAfter;
            this.posAfter = posAfter;
            this.postAfter = postAfter;
        }
    }
}